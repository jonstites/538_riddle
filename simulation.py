#!/usr/bin/env python3

import argh
import random
import math
from collections import Counter

def main(trials=100000, verbose=False):
    results = Counter()
    for t in range(trials):
        wait_time = sim_wait_time()
        results[wait_time] += 1
    show_median(results, trials)
    show_mode(results)
    show_mean(results)
    if verbose:
        show_results(results)

def sim_wait_time():
    time = 0
    i = new_task()
    j = new_task()
    while i != 0 or j != 0:
        if i == 0:
            i = new_task()
        if j == 0:
            j = new_task()
        i -= 1
        j -= 1
        time += 1
    return time

def show_median(results, trials):
    round_down = math.floor(trials/2)
    round_up = math.ceil(trials/2)
    total = 0
    for time, count in results.items():
        total += count
        if total > round_down:
            median_down = time
            break
    total = 0
    for time, count in results.items():
        total += count
        if total > round_up:
            median_up = time
            break
    median = (median_up + median_down)/2
    print("median wait time:", median)

def show_mode(results):
    print("modal wait time:", results.most_common(1)[0][0])

def show_mean(results):
    number = 0
    total_time = 0
    for time, count in results.items():
        total_time += time*count
        number += count
    print("mean wait time:", round(total_time/number, 4))

def new_task():
    return random.randrange(1, 5)

def show_results(results):
    for count, value in results.items():
        print(count, value, sep="\t")

if __name__ == "__main__":
    argh.dispatch_command(main)
